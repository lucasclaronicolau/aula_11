import React from 'react';

export default class Temperatura extends React.Component{

  onChange = (e) => {
    this.props.change(e.target.value)
  }

  render(){
    return(
      <div>
        <p>Velocidade:</p>
        <select name="velocidade" onChange = {this.onChange}>
          <option value = "1">1</option>
          <option value = "2">2</option>
          <option value  = "3">3</option>
          </select>
      </div>
    )
  }
}
