import React from 'react';
import Modo  from './Modo'
import Velocidade from './Velocidade'
import Temperatura from './Temperatura'

export default class Dados extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      temperatura : 15,
      modo : "Ventilar",
      velocidade : 1
    }
  }

  changeModo = (e) => {
    this.setState({
      modo : e
    })
  }

  changeTemperatura = (e) => {
    this.setState({
      temperatura : e
    })
  }

  changeVelocidade = (e) => {
    this.setState({
      velocidade : e
    })
  }

  render(){
    return(
      <div>
        <h1>Dados</h1>
        <p>Temperatura: {this.state.temperatura}</p>
        <p>Modo: {this.state.modo}</p>
        <p>Velocidade: {this.state.velocidade}</p>
        <hr />
        <Temperatura change={this.changeTemperatura}></Temperatura>
        <Modo change={this.changeModo}></Modo>
        <Velocidade change={this.changeVelocidade}></Velocidade>
      </div>
    )
  }
}
