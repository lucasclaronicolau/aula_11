import React from 'react';

export default class Temperatura extends React.Component{

  onChange = (e) => {
    this.props.change(e.target.value)
  }

  render(){
    return(
      <div>
        <p>Temperatura:</p>
        <input type="number" defaultValue="15" onChange = {this.onChange}></input>
      </div>
    )
  }
}
