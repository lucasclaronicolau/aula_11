import React from 'react';

export default class Temperatura extends React.Component{

  onChange = (e) => {
    this.props.change(e.target.value)
  }

  render(){
    return(
      <div>
        <p>Modo:</p>
        <select name="modo" onChange = {this.onChange}>
          <option value = "Ventilar">Ventilar</option>
          <option value = "Refrigerar">Refrigerar</option>
          <option value  = "Aquecer">Aquecer</option>
          </select>
      </div>
    )
  }
}
